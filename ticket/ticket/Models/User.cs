﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ticket.Models
{
    public class User : Account
    {
        [Required, MinLength(1)]
        public string Name { get; set; }

        [Required, MinLength(1)]
        public string Surname { get; set; }
        public string Patronymic { get; set; }

        [Required, DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }
    }
}
