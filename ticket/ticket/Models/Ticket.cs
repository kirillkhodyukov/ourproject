﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ticket.Models
{
    public class Ticket : IEntity<int>
    {
        public int Id { get; set; }
        public DateTime DateOfOrder { get { return DateTime.Now; } set { } }
        [Required]
        public decimal Price { get; set; }

        [ForeignKey("Route")]
        public int RouteId { get; set; }
        public virtual Route Route { get; set; }

        [Required]
        public DateTime DistpatchTime { get; set; }
        [Required]
        public DateTime ArrivalTime { get; set; }
        [Required]
        public int TicketCount { get; set; }


    }
}
