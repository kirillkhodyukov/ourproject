﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ticket.Models
{
    public abstract class Account : IEntity<int>
    {
        public int Id { get; set; }

        [Required, MinLength(4)]
        public string Login { get; set; }

        [Required, MinLength(4)]
        public string Password { get; set; }

        [Required, MinLength(4)]
        public string Email { get; set; }
        public DateTime DateOfCreation { get { return DateTime.Now; } set { } }

    }
}
