﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ticket.Enums;

namespace ticket.Models
{
    public class Order : IEntity<int>
    {
        public int Id { get; set; }
        public DateTime DateOfOrder { get { return DateTime.Now; } set { } }

        [Required, ForeignKey("User")]
        public int UserId { get; set; }
        public virtual User User { get; set; }
        [Required, ForeignKey("Ticket")]
        public int TicketId { get; set; }
        public virtual Ticket Ticket { get; set; }
        public int CountOfTicket { get; set; }

        [Required]
        public decimal TotalPrice { get; set; }

        [Required]
        public OrderStatus OrderStatus { get; set; }


    }
}
