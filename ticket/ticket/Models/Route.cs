﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ticket.Enums;

namespace ticket.Models
{
    public class Route : IEntity<int>
    {
        public int Id { get; set; }
        [Required]
        public TransportType TransportType { get; set; }
        [Required]
        public City DeparturePoint { get; set; }

        [Required]
        public City ArrivalPoint { get; set; }
    }
}
