﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ticket.Models;

namespace ticket
{
    
        public class ApplicationDbContext : DbContext
        {
            public DbSet<Account> Accounts { get; set; }
            public DbSet<User> Users { get; set; }
            public DbSet<Admin> Admins { get; set; }
            public DbSet<Order> Orders { get; set; }
            public DbSet<Route> Route { get; set; }
            public DbSet<Ticket> Ticket { get; set; }

            
           


            protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            {
                var buider = new ConfigurationBuilder();
                buider.SetBasePath(@"C:\repository\ourproject\ticket\ticket\");
                buider.AddJsonFile("appsettings.json");
                var config = buider.Build();

                var connectionString = config.GetConnectionString("DefaultConnection");

                optionsBuilder.UseSqlServer(connectionString);



            }

            protected override void OnModelCreating(ModelBuilder modelBuilder)
            {
                modelBuilder.Entity<Account>()
                     .HasIndex(x => x.Login)
                     .IsUnique();

                modelBuilder.Entity<Account>()
                    .HasIndex(x => x.Email)
                    .IsUnique();
            }
        }
    
}
