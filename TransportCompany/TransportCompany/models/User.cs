﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace TransportCompany.models
{
    public class User : Account
    {
        /// <summary>
        /// имя пользователя
        /// </summary>
        [Required]
        [MinLength(1)]
        public string Name { get; set; }

        /// <summary>
        /// фамилия пользователя
        /// </summary>
        [Required]
        [MinLength(1)]
        public string Surname { get; set; }

        /// <summary>
        /// отчество пользователя(необязательно поле)
        /// </summary>
        public string Patronymic { get; set; }

        /// <summary>
        /// дата рождения
        /// </summary>
        [Required]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }



        /// <summary>
        /// метод для добавления аккаунта пользователя
        /// </summary>
        /// <param name="db"></param>
        public static void AddUserAccount(ApplicationDbContext db)
        {
            Console.Clear();

            using (var transaction = db.Database.BeginTransaction())
            {
                User user = new User();

                user.IsAdmin = false;

                Console.WriteLine("Уникальные поля обозначаются как: *");
                Console.WriteLine("Обязательные поля обозначаются как: ^");
                Console.WriteLine("-------------------------------------");
                Console.WriteLine("Введите логин*^");
                Console.WriteLine("Минимальное количество символов 4");

                user.Login = Console.ReadLine();

                User u = db.Users.FirstOrDefault(x => x.Login == user.Login);
                if(u != null)
                {
                    Console.WriteLine("Пользователь с таким логином уже существует");
                    Console.WriteLine();
                    Console.WriteLine("-------------------------------------");
                    Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                    Console.ReadKey();
                    Console.Clear();
                    return;
                }

                user.Login = user.Login.Trim();

                while (user.Login.Length < 4)
                {
                    Console.WriteLine("----------------------------------");
                    Console.WriteLine("Минимальное количество символов 4");
                    Console.WriteLine("Введите логин повторно");
                    user.Login = Console.ReadLine();

                    u = db.Users.FirstOrDefault(x => x.Login == user.Login);
                    if (u != null)
                    {
                        Console.WriteLine("Пользователь с таким логином уже существует");
                        Console.WriteLine();
                        Console.WriteLine("-------------------------------------");
                        Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                        Console.ReadKey();
                        Console.Clear();
                        return;
                    }

                    user.Login = user.Login.Trim();
                }

                Console.WriteLine("----------------------------------");
                Console.WriteLine("Введите пароль*^");
                Console.WriteLine("Минимальное количество символов 4");

                user.Password = Console.ReadLine();

                while (user.Password.Length < 4)
                {
                    Console.WriteLine("----------------------------------");
                    Console.WriteLine("Минимальное количество символов 4");
                    Console.WriteLine("Введите пароль повторно");
                    user.Password = Console.ReadLine();
                }

                Console.WriteLine("----------------------------------");
                Console.WriteLine("Введите свою почту*^");
                Console.WriteLine("Минимальное количество символов 4");

                user.Email = Console.ReadLine();

                u = db.Users.FirstOrDefault(x => x.Email == user.Email);
                if (u != null)
                {
                    Console.WriteLine("Пользователь с такой почтой уже существует");
                    Console.WriteLine();
                    Console.WriteLine("-------------------------------------");
                    Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                    Console.ReadKey();
                    Console.Clear();
                    return;
                }

                user.Email = user.Email.Trim();

                while (user.Email.Length < 4)
                {
                    Console.WriteLine("----------------------------------");
                    Console.WriteLine("Минимальное количество символов 4");
                    Console.WriteLine("Введите почту повторно");
                    user.Email = Console.ReadLine();

                    u = db.Users.FirstOrDefault(x => x.Login == user.Login);
                    if (u != null)
                    {
                        Console.WriteLine("Пользователь с такой почтой уже существует");
                        Console.WriteLine();
                        Console.WriteLine("-------------------------------------");
                        Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                        Console.ReadKey();
                        Console.Clear();
                        return;
                    }

                    user.Email = user.Email.Trim();
                }

                Console.WriteLine("----------------------------------");
                Console.WriteLine("Введите своё имя^");
                Console.WriteLine("Минимальное количество символов 1");

                user.Name = Console.ReadLine();
                user.Name = user.Name.Trim();

                while (user.Name.Length < 1)
                {
                    Console.WriteLine("----------------------------------");
                    Console.WriteLine("Минимальное количество символов 1");
                    Console.WriteLine("Введите почту повторно");
                    user.Name = Console.ReadLine();
                    user.Name = user.Name.Trim();
                }

                Console.WriteLine("----------------------------------");
                Console.WriteLine("Введите своё фамилию^");
                Console.WriteLine("Минимальное количество символов 1");

                user.Surname = Console.ReadLine();
                user.Surname = user.Surname.Trim();

                while (user.Surname.Length < 1)
                {
                    Console.WriteLine("----------------------------------");
                    Console.WriteLine("Минимальное количество символов 1");
                    Console.WriteLine("Введите фамилию повторно");
                    user.Surname = Console.ReadLine();
                    user.Surname = user.Surname.Trim();
                }

                Console.WriteLine("----------------------------------");
                Console.WriteLine("Введите своё отчество");

                user.Patronymic = Console.ReadLine();
                user.Patronymic = user.Patronymic.Trim();

                Console.WriteLine("---------------------------------------------");
                Console.WriteLine("Введите свою дату рождения в формате гггг-мм-дд");

                user.DateOfBirth = TryParseDateTime();

                while (user.DateOfBirth > DateTime.Now || user.DateOfBirth < DateTime.Now.AddYears(-200))
                {
                    if (user.DateOfBirth > DateTime.Now)
                    {
                        Console.WriteLine("------------------------------------------------");
                        Console.WriteLine("Дата рождения не может быть больше текущей даты");
                        Console.WriteLine("Введите свою дату рождения в формате гггг-мм-дд");
                        user.DateOfBirth = TryParseDateTime();
                    }

                    if (user.DateOfBirth < DateTime.Now.AddYears(-200))
                    {
                        Console.WriteLine("---------------------------------------------");
                        Console.WriteLine("Вам не может быть больше 200 лет");
                        Console.WriteLine("Введите свою дату рождения в формате гггг-мм-дд");
                        user.DateOfBirth = TryParseDateTime();
                    }
                }

                db.Add(user);
                db.SaveChanges();

                transaction.Commit();

                Console.WriteLine("Пользователь добавлен");
                Console.WriteLine();
                Console.WriteLine("-------------------------------------");
                Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                Console.ReadKey();
                Console.Clear();
            }
        }

        /// <summary>
        /// проверка на возможность перевести string в datetime
        /// </summary>
        /// <returns></returns>
        public static DateTime TryParseDateTime()
        {
            DateTime i;
            bool parse = DateTime.TryParse(Console.ReadLine(), out i);
            while (parse == false)
            {
                Console.WriteLine("----------------------------------------------");
                Console.WriteLine("Неверный ввод. Введите дату в формате гггг-мм-дд");
                parse = DateTime.TryParse(Console.ReadLine(), out i);
            }
            return i;
        }
    }
}
