﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using TransportCompany.enums;
using TransportCompany.interfces;

namespace TransportCompany.models
{
    public class Ticket : IEntity<int>
    {
        public int ID { get; set; }

        /// <summary>
        /// id маршрута
        /// </summary>
        [ForeignKey("Route")]
        public int RouteID { get; set; }

        /// <summary>
        /// дата создания билета
        /// </summary>
        public DateTime DateOfOrder
        {
            get
            {
                return DateTime.Now;
            }
            set { }
        }

        /// <summary>
        /// время отправки
        /// </summary>
        [Required]
        public DateTime DispatchTime { get; set; }

        /// <summary>
        /// время прибытия
        /// </summary>
        [Required]
        public DateTime ArrivalTime { get; set; }


        /// <summary>
        /// цена одного билета
        /// </summary>
        [Required]
        public decimal Price { get; set; }

        /// <summary>
        /// количество доступных билетов на рейс
        /// </summary>
        [Required]
        public int TicketCount { get; set; }


        /// <summary>
        /// навигационные свойства
        /// </summary>
        public virtual Route Route { get; set; }



        /// <summary>
        /// обычный вывод информации без фильтров для пользователя
        /// </summary>
        /// <param name="db"></param>
        /// <param name="id">передаётся ID аккаунта</param>
        public static void TicketInfoForAcc(ApplicationDbContext db, int id)
        {
            Console.Clear();

            var acc = db.Accounts.FirstOrDefault(x => x.ID == id);

            List<Ticket> tickets = null;

            if (acc.IsAdmin == true)
                tickets = db.Tickets
                    .Include(t => t.Route).ToList();
            else
                tickets = db.Tickets
                    .Include(t => t.Route)
                    .Where(t => t.DispatchTime > DateTime.Now
                            && t.TicketCount > 0)
                    .ToList();

            if (tickets is null)
            {
                Console.WriteLine("Билетов нет");
                Console.WriteLine();
                Console.WriteLine("-------------------------------------");
                Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                Console.ReadKey();
                Console.Clear();
                return;
            }

            PrintInfoList(tickets);

            Console.WriteLine();
            Console.WriteLine("-------------------------------------");
            Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
            Console.ReadKey();
            Console.Clear();
        }

        /// <summary>
        /// вывод информации c фильтрами для пользователя
        /// </summary>
        /// <param name="db"></param>
        /// <param name="id">передаётся ID аккаунта</param>
        public static void TicketInfoForAccWithFilters(ApplicationDbContext db, int id)
        {
            Console.Clear();

            List<Ticket> tickets = null;

            var acc = db.Accounts.FirstOrDefault(x => x.ID == id);

            if (acc.IsAdmin == true)
                tickets = db.Tickets
                    .Include(t => t.Route)
                    .ToList();
            else
                tickets = db.Tickets
                    .Include(t => t.Route)
                    .Where(t => t.DispatchTime > DateTime.Now
                            && t.TicketCount > 0)
                    .ToList();

            if (tickets is null)
            {
                Console.WriteLine("Билетов нет");
                Console.WriteLine();
                Console.WriteLine("-------------------------------------");
                Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                Console.ReadKey();
                Console.Clear();
                return;
            }

            Console.WriteLine("Введите номер нужного фильтра");
            Console.WriteLine("1. Точка отправки");
            Console.WriteLine("2. Точка прибытия");
            Console.WriteLine("3. Тип транспорта");
            Console.WriteLine("4. Фильтр по цене");
            Console.WriteLine("5. Фильтр минимальной цены");
            Console.WriteLine("6. Фильтр максимальной цены");
            Console.WriteLine("7. Фильтр по количеству времени");
            Console.WriteLine("8. Ближайшие рейсы");
            Console.WriteLine("9. Выйти");

            //переменная для интерфейса
            int input = TryParseInt();

            while (input < 1 || input > 9)
            {
                Console.WriteLine("-----------------------------");
                Console.WriteLine("Некорректрный ввод");
                Console.WriteLine("Введите номер нужного фильтра");
                Console.WriteLine("1. Точка отправки");
                Console.WriteLine("2. Точка прибытия");
                Console.WriteLine("3. Тип транспорта");
                Console.WriteLine("4. Фильтр по цене");
                Console.WriteLine("5. Фильтр минимальной цены");
                Console.WriteLine("6. Фильтр максимальной цены");
                Console.WriteLine("7. Фильтр по количеству времени");
                Console.WriteLine("8. Ближайшие рейсы");
                Console.WriteLine("9. Выйти");
                input = TryParseInt();
            }

            //переменная для вариантов выбора
            int option;

            while (input > 0 && input <= 9)
            {
                switch (input)
                {
                    case 1:

                        List<City> city = new List<City>();

                        foreach (var t in tickets)
                        {
                            city.Add(t.Route.DepartureCity);
                        }

                        city = city.Distinct().ToList();

                        Console.WriteLine("Введите точку отправки");

                        foreach (var value in city)
                        {
                            Console.WriteLine("{0}.  {1}",
                                (int)value, (value));
                        }

                        option = TryParseInt();

                        foreach (int i in city)
                        {
                            if (option != i)
                                continue;
                            else
                                tickets = tickets
                                        .Where(t => t.Route.DepartureCity == (City)option)
                                        .ToList();
                        }

                        while (option > Enum.GetValues(typeof(City)).Length
                                    || option <= 0)
                        {
                            Console.WriteLine("----------------------------------");
                            Console.WriteLine("Такой точки отправки не сущестует");
                            Console.WriteLine("Введите номер точки отправки снова");

                            foreach (var value in city)
                            {
                                Console.WriteLine("{0}.  {1}",
                                   (int)value, (value));
                            }

                            option = TryParseInt();

                            foreach (int i in city)
                            {
                                if (option != i)
                                    continue;
                                else
                                    tickets = tickets
                                            .Where(t => t.Route.DepartureCity == (City)option)
                                            .ToList(); ;
                            }
                        }

                        PrintInfoList(tickets);
                        break;

                    case 2:
                        List<City> city1 = new List<City>();

                        foreach (var t in tickets)
                        {
                            city1.Add(t.Route.ArrivalCity);
                        }

                        city1 = city1.Distinct().ToList();

                        Console.WriteLine("Введите точку прибытия");

                        foreach (var value in city1)
                        {
                            Console.WriteLine("{0}.  {1}",
                                (int)value, (value));
                        }

                        option = TryParseInt();

                        foreach (int i in city1)
                        {
                            if (option != i)
                                continue;
                            else
                                tickets = tickets
                                        .Where(t => t.Route.ArrivalCity == (City)option)
                                        .ToList();
                        }

                        while (option > Enum.GetValues(typeof(City)).Length
                                || option <= 0)
                        {
                            Console.WriteLine("----------------------------------");
                            Console.WriteLine("Такой точки прибытия не сущестует");
                            Console.WriteLine("Введите номер точки прибытия снова");
                            foreach (var value in city1)
                            {
                                Console.WriteLine("{0}.  {1}",
                                   (int)value, (value));
                            }

                            option = TryParseInt();

                            foreach (int i in city1)
                            {
                                if (option != i)
                                    continue;
                                else
                                    tickets = tickets
                                            .Where(t => t.Route.ArrivalCity == (City)option)
                                            .ToList();
                            }
                        }

                        PrintInfoList(tickets);
                        break;

                    case 3:
                        List<TransportType> tType = new List<TransportType>();

                        foreach (var t in tickets)
                        {
                            tType.Add(t.Route.TransportType);
                        }

                        tType = tType.Distinct().ToList();

                        Console.WriteLine("Введите номер вид транспорта");
                        foreach (var value in tType)
                        {
                            Console.WriteLine("{0}.  {1}",
                                (int)value, (value));
                        }

                        option = TryParseInt();

                        foreach (int i in tType)
                        {
                            if (option != i)
                                continue;
                            else
                                tickets = tickets
                                        .Where(t => t.Route.TransportType == (TransportType)option)
                                        .ToList();
                        }

                        while (option > Enum.GetValues(typeof(City)).Length
                                    || option <= 0)
                        {
                            Console.WriteLine("----------------------------------");
                            Console.WriteLine("Такого вида транспорта не сущестует");
                            Console.WriteLine("Введите номер вида транспорта снова");

                            foreach (var value in tType)
                            {
                                Console.WriteLine("{0}.  {1}",
                                   (int)value, (value));
                            }

                            option = TryParseInt();

                            foreach (int i in tType)
                            {
                                if (option != i)
                                    continue;
                                else
                                    tickets = tickets
                                            .Where(t => t.Route.TransportType == (TransportType)option)
                                            .ToList();
                            }
                        }

                        PrintInfoList(tickets);
                        break;

                    case 4:
                        Console.WriteLine("1. Сортировка по возрастанию цены");
                        Console.WriteLine("2. Сортировка по убыванию цены");
                        option = TryParseInt();

                        while (option != 1 && option != 2)
                        {
                            Console.WriteLine("---------------------------");
                            Console.WriteLine("Неверный выбор варианта");
                            Console.WriteLine("1. Сортировка по возрастанию цены");
                            Console.WriteLine("2. Сортировка по убыванию цены");
                            option = TryParseInt();
                        }

                        if (option == 1)
                        {
                            tickets = tickets
                                    .OrderBy(x => x.Price)
                                    .ToList();

                            PrintInfoList(tickets);
                        }

                        if (option == 2)
                        {
                            tickets = tickets
                                    .OrderByDescending(x => x.Price)
                                    .ToList();

                            PrintInfoList(tickets);
                        }
                        break;

                    case 5:
                        Console.WriteLine("Введите минимальную цену");
                        option = TryParseInt();

                        if (option < 0)
                        {
                            Console.WriteLine("Минимальная цена не может быть меньше нуля");
                            break;
                        }

                        tickets = tickets
                                .Where(x => x.Price >= option)
                                .ToList();

                        PrintInfoList(tickets);
                        break;

                    case 6:
                        Console.WriteLine("Введите максимальную цену");
                        option = TryParseInt();

                        if (option < 0)
                        {
                            Console.WriteLine("Максимальная цена не может быть меньше нуля");
                            break;
                        }

                        tickets = tickets
                                .Where(x => x.Price <= option)
                                .ToList();

                        PrintInfoList(tickets);
                        break;

                    case 7:
                        tickets = tickets
                                .OrderBy(x => x.ArrivalTime - x.DispatchTime)
                                .ToList();

                        PrintInfoList(tickets);
                        break;

                    case 8:
                        tickets = tickets
                                .OrderBy(x => x.DispatchTime - DateTime.Now)
                                .ToList();

                        PrintInfoList(tickets);
                        break;

                    default:
                        Console.Clear();
                        return;
                }

                Console.WriteLine();
                Console.WriteLine("-------------------------------------");
                Console.WriteLine("Введите ещё раз номер нужного фильтра");
                Console.WriteLine("1. Точка отправки");
                Console.WriteLine("2. Точка прибытия");
                Console.WriteLine("3. Тип транспорта");
                Console.WriteLine("4. Фильтр по цене");
                Console.WriteLine("5. Фильтр минимальной цены");
                Console.WriteLine("6. Фильтр максимальной цены");
                Console.WriteLine("7. Фильтр по количеству времени");
                Console.WriteLine("8. Ближайшие рейсы");
                Console.WriteLine("9. Выйти");
                input = TryParseInt();

                while (input < 1 || input > 9)
                {
                    Console.WriteLine("-----------------------------");
                    Console.WriteLine("Некорректрный ввод");
                    Console.WriteLine("Введите номер нужного фильтра");
                    Console.WriteLine("1. Точка отправки");
                    Console.WriteLine("2. Точка прибытия");
                    Console.WriteLine("3. Тип транспорта");
                    Console.WriteLine("4. Фильтр по цене");
                    Console.WriteLine("5. Фильтр минимальной цены");
                    Console.WriteLine("6. Фильтр максимальной цены");
                    Console.WriteLine("7. Фильтр по количеству времени");
                    Console.WriteLine("8. Ближайшие рейсы");
                    Console.WriteLine("9. Выйти");
                    input = TryParseInt();
                }

                Console.Clear();
            }
        }

        /// <summary>
        /// метод добаления билетов
        /// </summary>
        /// <param name="db"></param>
        public static void AddTicket(ApplicationDbContext db)
        {
            Console.Clear();

            using (var transaction = db.Database.BeginTransaction())
            {
                Ticket ticket = new Ticket();
                Console.WriteLine("Обязательные поля обозначаются как: ^");
                Console.WriteLine();

                var routes = db.Routes.ToList();

                if(routes is null)
                {
                    Console.WriteLine("Маршрутов нет. Добавьте маршруты");
                    Console.WriteLine();
                    Console.WriteLine("-------------------------------------");
                    Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                    Console.ReadKey();
                    Console.Clear();
                    return;
                }

                foreach (var r in routes)
                {
                    Console.WriteLine("-----------------------------");
                    Console.WriteLine($"ID маршрута: {r.ID}");
                    Console.WriteLine($"Город отправки: {r.DepartureCity}");
                    Console.WriteLine($"Город прибытия: {r.ArrivalCity}");
                    Console.WriteLine($"Тип транспорта: {r.TransportType}");
                }

                Console.WriteLine();

                Console.WriteLine("Введите номер маршрута(ID)^");

                int id = TryParseInt();

                var route = routes.FirstOrDefault(x => x.ID == id);

                if (route is null)
                {
                    Console.WriteLine("Такого маршрута не существует");
                    Console.WriteLine();
                    Console.WriteLine("-------------------------------------");
                    Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                    Console.ReadKey();
                    Console.Clear();
                    return;
                }

                ticket.RouteID = route.ID;

                Console.WriteLine("----------------------------------");
                Console.WriteLine("Введите цену^");
                ticket.Price = TryParseInt();

                while (ticket.Price < 0)
                {
                    Console.WriteLine("Цена не может быть ниже нуля");
                    Console.WriteLine("Введите цену заново");
                    ticket.Price = TryParseInt();
                }

                Console.WriteLine("----------------------------------------");
                Console.WriteLine("Введите дату отправки в формате гггг-мм-дд");
                ticket.DispatchTime = TryParseDateTime();

                Console.WriteLine("Введите часы отправки");
                int hour = TryParseInt();
                while(hour < 0 || hour > 23)
                {
                    Console.WriteLine("---------------------");
                    Console.WriteLine("Некорректный ввод");
                    Console.WriteLine("Введите часы отправки");
                    hour = TryParseInt();
                }

                Console.WriteLine("Введите минуты отправки");
                int minute = TryParseInt();
                while(minute < 0 || minute > 59)
                {
                    Console.WriteLine("---------------------");
                    Console.WriteLine("Некорректный ввод");
                    Console.WriteLine("Введите часы отправки");
                    minute = TryParseInt();
                }

                ticket.DispatchTime = ticket.DispatchTime.AddHours(hour);
                ticket.DispatchTime = ticket.DispatchTime.AddMinutes(minute);

                while (ticket.DispatchTime < DateTime.Now.AddDays(3))
                {
                    Console.WriteLine("----------------------------------------------------------------");
                    Console.WriteLine("Нельзя добавить билет, срок которого уже истёк или скоро истечёт");
                    Console.WriteLine("Введите дату отправки в формате гггг-мм-дд заново");
                    ticket.DispatchTime = TryParseDateTime();

                    Console.WriteLine("Введите часы отправки");
                    hour = TryParseInt();
                    while (hour < 0 || hour > 23)
                    {
                        Console.WriteLine("---------------------");
                        Console.WriteLine("Некорректный ввод");
                        Console.WriteLine("Введите часы отправки");
                        hour = TryParseInt();
                    }

                    Console.WriteLine("Введите минуты отправки");
                    minute = TryParseInt();
                    while (minute < 0 || minute > 59)
                    {
                        Console.WriteLine("---------------------");
                        Console.WriteLine("Некорректный ввод");
                        Console.WriteLine("Введите минуты отправки");
                        minute = TryParseInt();
                    }

                    ticket.DispatchTime = ticket.DispatchTime.AddHours(hour);
                    ticket.DispatchTime = ticket.DispatchTime.AddMinutes(minute);
                }

                Console.WriteLine("--------------------------------------------------");
                Console.WriteLine("Введите дату прибытия в формате гггг-мм-дд");
                ticket.ArrivalTime = TryParseDateTime();

                Console.WriteLine("Введите часы прибытия");
                hour = TryParseInt();
                while (hour < 0 || hour > 23)
                {
                    Console.WriteLine("---------------------");
                    Console.WriteLine("Некорректный ввод");
                    Console.WriteLine("Введите часы прибытия");
                    hour = TryParseInt();
                }

                Console.WriteLine("Введите минуты прибытия");
                minute = TryParseInt();
                while (minute < 0 || minute > 59)
                {
                    Console.WriteLine("---------------------");
                    Console.WriteLine("Некорректный ввод");
                    Console.WriteLine("Введите минуты прибытия");
                    minute = TryParseInt();
                }

                ticket.ArrivalTime = ticket.ArrivalTime.AddHours(hour);
                ticket.ArrivalTime = ticket.ArrivalTime.AddMinutes(minute);

                while (ticket.ArrivalTime < ticket.DispatchTime)
                {
                    Console.WriteLine("------------------------------------------------------------------");
                    Console.WriteLine("Нельзя добавить билет, дата отправки которого больше даты прибытия");
                    Console.WriteLine("Введите дату прибытия в формате гг-мм-дд заново");
                    ticket.ArrivalTime = TryParseDateTime();

                    Console.WriteLine("Введите часы прибытия");
                    hour = TryParseInt();
                    while (hour < 0 || hour > 23)
                    {
                        Console.WriteLine("---------------------");
                        Console.WriteLine("Некорректный ввод");
                        Console.WriteLine("Введите часы прибытия");
                        hour = TryParseInt();
                    }

                    Console.WriteLine("Введите минуты прибытия");
                    minute = TryParseInt();
                    while (minute < 0 || minute > 59)
                    {
                        Console.WriteLine("---------------------");
                        Console.WriteLine("Некорректный ввод");
                        Console.WriteLine("Введите минуты прибытия");
                        minute = TryParseInt();
                    }

                    ticket.ArrivalTime = ticket.ArrivalTime.AddHours(hour);
                    ticket.ArrivalTime = ticket.ArrivalTime.AddMinutes(minute);
                }

                Console.WriteLine("-----------------------------------------------");
                Console.WriteLine("Введите доступное количество билетов");
                ticket.TicketCount = TryParseInt();

                while (ticket.TicketCount <= 0)
                {
                    Console.WriteLine("-----------------------------------------------------------------");
                    Console.WriteLine("Доступное количество билетов не может быть меньше или равно нулю");
                    Console.WriteLine("Введите доступное количество билетов");
                    ticket.TicketCount = TryParseInt();
                }

                db.Add(ticket);
                db.SaveChanges();

                transaction.Commit();

                Console.WriteLine("Билеты добавлены");
                Console.WriteLine();
                Console.WriteLine("-------------------------------------");
                Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                Console.ReadKey();
                Console.Clear();
            }
        }

        /// <summary>
        /// метод для покупки билетов пользователем
        /// </summary>
        /// <param name="db"></param>
        /// <param name="id">передаётся ID текущего пользователя</param>
        public static void ByeTicket(ApplicationDbContext db, int id)
        {
            Console.Clear();

            Console.WriteLine("Введите минимальную дату оптравки в формате гггг-мм-дд");

            DateTime DispatchDay = TryParseDateTime();

            Console.WriteLine("Введите максимальную дату прибытия в формате гггг-мм-дд");

            DateTime ArrivalDay = TryParseDateTime();

            while (ArrivalDay < DispatchDay)
            {
                Console.WriteLine("-------------------------------------------------------------------------");
                Console.WriteLine("Максимальная дата прибытия не может быть меньше минимальной даты отправки");
                ArrivalDay = TryParseDateTime();
            }

            var tickets = db.Tickets
                            .Where(x => x.DispatchTime > DispatchDay
                                     && x.ArrivalTime < ArrivalDay)
                            .ToList();

            if (tickets is null)
            {
                Console.WriteLine("Билетов между этими датами не существует");
                return;
            }

            Console.WriteLine();
            PrintInfoList(tickets);

            Console.WriteLine("Введите ID билета");
            //временная изменяющаяся переменная для интерфейса
            int input = TryParseInt();

            Ticket ticket = tickets.FirstOrDefault(x => x.ID == input);

            if (ticket is null)
            {
                Console.WriteLine("Билета с таким ID не существует");
                Console.WriteLine();
                Console.WriteLine("-------------------------------------");
                Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                Console.ReadKey();
                Console.Clear();
                return;
            }

            if (ticket.TicketCount == 0)
            {
                Console.WriteLine("Количество доступных билетов на этот рейс 0");
                Console.WriteLine();
                Console.WriteLine("-------------------------------------");
                Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                Console.ReadKey();
                Console.Clear();
                return;
            }

            if (ticket.DispatchTime < DateTime.Now)
            {
                Console.WriteLine("Этот билет не доступен к покупке из-за текущего времени");
                Console.WriteLine();
                Console.WriteLine("-------------------------------------");
                Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                Console.ReadKey();
                Console.Clear();
                return;
            }

            User user = db.Users.FirstOrDefault(x => x.ID == id);

            Console.WriteLine("Введите количество покупаемых билетов");
            input = TryParseInt();
            while (input <= 0 || input > ticket.TicketCount)
            {
                if (input <= 0)
                {
                    Console.WriteLine("------------------------------------------------------------------------");
                    Console.WriteLine("Количестов покупаемых билетов не может быть отрицательным или равно нулю");
                    Console.WriteLine("Введите количество покупаемых билетов заново");
                    input = TryParseInt();
                }
                if (input > ticket.TicketCount)
                {
                    Console.WriteLine("---------------------------------------------------------------------");
                    Console.WriteLine("Количество покупаемых билетов не может превышать количество имеющихся");
                    Console.WriteLine($"В данный момент есть: {ticket.TicketCount} билетов");
                    Console.WriteLine("Введите количество покупаемых билетов заново");
                    input = TryParseInt();
                }
            }

            ticket.TicketCount -= input;

            Order order = new Order
            {
                UserID = user.ID,
                TicketID = ticket.ID,
                CountOfTicket = input,
                TotalPrice = input * ticket.Price,
                OrderStatus = OrderStatus.Bought
            };

            db.Add(order);
            db.Update(ticket);
            db.SaveChanges();

            Console.WriteLine("Билеты купленны");
            Console.WriteLine();
            Console.WriteLine("-------------------------------------");
            Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
            Console.ReadKey();
            Console.Clear();
        }

        /// <summary>
        /// метод резервации билетов
        /// </summary>
        /// <param name="db"></param>
        /// <param name="id">передаётся ID текущего пользователя</param>
        public static void TicketReservation(ApplicationDbContext db, int id)
        {
            Console.Clear();

            Console.WriteLine("Введите минимальную дату оптравки в формате гггг-мм-дд");

            DateTime DispatchDay = TryParseDateTime();

            Console.WriteLine("Введите максимальную дату прибытия в формате гггг-мм-дд");

            DateTime ArrivalDay = TryParseDateTime();

            while (ArrivalDay < DispatchDay)
            {
                Console.WriteLine("-------------------------------------------------------------------------");
                Console.WriteLine("Максимальная дата прибытия не может быть меньше минимальной даты отправки");
                ArrivalDay = TryParseDateTime();
            }

            var tickets = db.Tickets
                            .Where(x => x.DispatchTime > DispatchDay
                                     && x.ArrivalTime < ArrivalDay)
                            .ToList();

            if (tickets is null)
            {
                Console.WriteLine("Билетов между этими датами не существует");
                return;
            }

            Console.WriteLine();
            PrintInfoList(tickets);

            Console.WriteLine("Введите ID билета");
            //локальная переменная int для интерфейса(будет изменяться с каждым вводом)
            int input = TryParseInt();

            Ticket ticket = tickets.FirstOrDefault(x => x.ID == input);

            if (ticket is null)
            {
                Console.WriteLine("Билета с таким ID не существует");
                Console.WriteLine();
                Console.WriteLine("-------------------------------------");
                Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                Console.ReadKey();
                Console.Clear();
                return;
            }

            if (ticket.TicketCount == 0)
            {
                Console.WriteLine("Количество доступных билетов на этот рейс 0");
                Console.WriteLine();
                Console.WriteLine("-------------------------------------");
                Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                Console.ReadKey();
                Console.Clear();
                return;
            }

            if (ticket.DispatchTime < DateTime.Now.AddDays(3))
            {
                Console.WriteLine("Этот билет не доступен к резервации из-за текущего времени");
                Console.WriteLine();
                Console.WriteLine("-------------------------------------");
                Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                Console.ReadKey();
                Console.Clear();
                return;
            }

            User user = db.Users.FirstOrDefault(x => x.ID == id);

            Console.WriteLine("Введите количество резервиемых билетов");
            input = TryParseInt();
            while (input <= 0 || input > ticket.TicketCount)
            {
                if (input <= 0)
                {
                    Console.WriteLine("---------------------------------------------------------------------------");
                    Console.WriteLine("Количестов резервируемых билетов не может быть отрицательным или равно нулю");
                    Console.WriteLine("Введите количество резервируемых билетов заново");
                    input = TryParseInt();
                }
                if (input > ticket.TicketCount)
                {
                    Console.WriteLine("------------------------------------------------------------------------");
                    Console.WriteLine("Количество резервируемых билетов не может превышать количество имеющихся");
                    Console.WriteLine($"В данный момент есть: {ticket.TicketCount} билетов");
                    Console.WriteLine("Введите количество резервируемых билетов заново");
                    input = TryParseInt();
                }

                ticket.TicketCount -= input;

                Order order1 = new Order
                {
                    UserID = user.ID,
                    TicketID = ticket.ID,
                    CountOfTicket = input,
                    TotalPrice = input * ticket.Price,
                    OrderStatus = OrderStatus.Reserved
                };

                db.Add(order1);
                db.Update(ticket);
                db.SaveChanges();

                Console.WriteLine("Билеты зарезервированы");
                Console.WriteLine();
                Console.WriteLine("-------------------------------------");
                Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                Console.ReadKey();
                Console.Clear();
                return;
            }

            ticket.TicketCount -= input;

            Order order = new Order
            {
                UserID = user.ID,
                TicketID = ticket.ID,
                CountOfTicket = input,
                TotalPrice = input * ticket.Price,
                OrderStatus = OrderStatus.Reserved
            };

            db.Add(order);
            db.Update(ticket);
            db.SaveChanges();

            Console.WriteLine("Билеты зарезервированы");
            Console.WriteLine();
            Console.WriteLine("-------------------------------------");
            Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
            Console.ReadKey();
            Console.Clear();
        }

        /// <summary>
        /// метод поиска лучшего маршрута по условиям
        /// </summary>
        /// <param name="db"></param>
        public static void BestRoute(ApplicationDbContext db)
        {
            Console.Clear();

            //лист билетов без пересадок
            var tickets = db.Tickets
               .Include(t => t.Route)
               .Where(t => t.DispatchTime > DateTime.Now
                       && t.TicketCount > 0)
               .ToList();

            if (tickets is null)
            {
                Console.WriteLine("Билетов нет");
                Console.WriteLine();
                Console.WriteLine("-------------------------------------");
                Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                Console.ReadKey();
                Console.Clear();
                return;
            }

            Console.WriteLine("Введите номер поиска");
            Console.WriteLine("1. По цене");
            Console.WriteLine("2. По времени");
            int option = TryParseInt();

            while (option != 1 && option != 2)
            {
                Console.WriteLine("--------------------");
                Console.WriteLine("Некорректный ввод");
                Console.WriteLine("Введите номер поиска");
                Console.WriteLine("1. По цене");
                Console.WriteLine("2. По времени");
            }

            if (option == 1)
            {
                List<City> city1 = new List<City>();

                foreach (var t in tickets)
                {
                    city1.Add(t.Route.DepartureCity);
                }

                city1.Distinct();

                Console.WriteLine("Введите точку отправки");

                foreach (var value in Enum.GetValues(typeof(City)))
                {
                    Console.WriteLine("{0}.  {1}",
                        (int)value, (value));
                }

                int option1 = TryParseInt();

                foreach (int i in city1)
                {
                    if (option1 != i)
                        continue;
                    else
                        tickets = tickets
                                .Where(t => t.Route.DepartureCity == (City)option1)
                                .ToList();
                }

                while (option1 > Enum.GetValues(typeof(City)).Length || option1 <= 0)
                {
                    Console.WriteLine("----------------------------------");
                    Console.WriteLine("Такой точки отправки не сущестует");
                    Console.WriteLine("Введите номер точки отправки снова");
                    foreach (var value in Enum.GetValues(typeof(City)))
                    {
                        Console.WriteLine("{0}.  {1}",
                           (int)value, (value));
                    }

                    option1 = TryParseInt();

                    foreach (int i in city1)
                    {
                        if (option1 != i)
                            continue;
                        else
                            tickets = tickets
                                    .Where(t => t.Route.DepartureCity == (City)option1)
                                    .ToList();
                    }
                }

                //лист билетов для пересадок
                var tickets2 = db.Tickets
                    .Include(t => t.Route)
                    .Where(t => t.DispatchTime > DateTime.Now
                         && t.TicketCount > 0
                         && t.Route.DepartureCity == (City)option1)
                    .ToList();

                if (tickets2 is null)
                {
                    Console.WriteLine("Таких билетов нет");
                    Console.WriteLine();
                    Console.WriteLine("-------------------------------------");
                    Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                    Console.ReadKey();
                    Console.Clear();
                    return;
                }

                List<City> city2 = new List<City>();

                foreach (var t in tickets)
                {
                    city2.Add(t.Route.ArrivalCity);
                }

                city2.Distinct();

                Console.WriteLine("Введите точку прибытия");
                foreach (var value in Enum.GetValues(typeof(City)))
                {
                    Console.WriteLine("{0}.  {1}",
                        (int)value, (value));
                }

                int option2 = TryParseInt();

                foreach (int i in city2)
                {
                    if (option2 != i)
                        continue;
                    else
                        tickets = tickets
                                .Where(t => t.Route.ArrivalCity == (City)option2)
                                .ToList();
                }

                while (option2 > Enum.GetValues(typeof(City)).Length || option2 <= 0)
                {
                    Console.WriteLine("----------------------------------");
                    Console.WriteLine("Такой точки прибытия не сущестует");
                    Console.WriteLine("Введите номер точки прибытия снова");
                    foreach (var value in Enum.GetValues(typeof(City)))
                    {
                        Console.WriteLine("{0}.  {1}",
                           (int)value, (value));
                    }

                    option2 = TryParseInt();

                    foreach (int i in city1)
                    {
                        if (option2 != i)
                            continue;
                        else
                            tickets = tickets
                                    .Where(t => t.Route.ArrivalCity == (City)option2)
                                    .ToList();
                    }
                }

                //лист билетов для пересадок
                var tickets3 = db.Tickets
                                .Include(t => t.Route)
                                .Where(t => t.DispatchTime > DateTime.Now
                                        && t.TicketCount > 0
                                        && t.Route.ArrivalCity == (City)option2)
                                .ToList();

                if (tickets3 is null)
                {
                    Console.WriteLine("Таких билетов нет");
                    Console.WriteLine();
                    Console.WriteLine("-------------------------------------");
                    Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                    Console.ReadKey();
                    Console.Clear();
                    return;
                }

                if (tickets != null)
                {
                    tickets = tickets
                            .OrderBy(x => x.Price)
                            .ToList();
                    var t = tickets.FirstOrDefault(x => x.Price > -1
                                                    && x.Route.ArrivalCity == (City)option2);
                    if (t != null)
                    {
                        Console.WriteLine("----------------------------------------");
                        Console.WriteLine("Лучшее предложение по цене без пересадок");
                        PrintInfoOne(t);
                    }
                }

                tickets2 = tickets2
                        .OrderBy(x => x.Price)
                        .ToList();
                tickets3 = tickets3
                        .OrderBy(x => x.Price)
                        .ToList();

                //для вывода лучшего маршрута с пересадками
                Ticket tick1 = null;
                Ticket tick2 = null;
                //точка отправки
                Ticket t1 = null;
                //точка прибытия
                Ticket t2 = null;
                decimal MinPrice = 9999999999999999999;

                foreach (var t in tickets2)
                {
                    t2 = tickets3.FirstOrDefault(x => t.Route.ArrivalCity == x.Route.DepartureCity
                                                    && t.ArrivalTime.AddHours(1) <= x.DispatchTime);
                    if (t2 != null)
                    {
                        t1 = t;
                        if (MinPrice > t2.Price + t1.Price)
                        {
                            MinPrice = t2.Price + t1.Price;
                            tick2 = t2;
                            tick1 = t1;
                        }
                    }
                }

                if (tick1 != null && tick2 != null)
                {
                    Console.WriteLine("-----------------------------------------");
                    Console.WriteLine("Лучшее предложение по цене с 1 пересадкой");
                    PrintInfoOne(tick1);
                    Console.WriteLine();
                    PrintInfoOne(tick2);
                    Console.WriteLine();
                    Console.WriteLine($"Минимальная цена с 1 пересадками: {MinPrice}");
                }

                //промежуточный лист билетов для 2 пересадок
                var tickets4 = db.Tickets
                        .Where(x => x.DispatchTime > DateTime.Now
                                && x.TicketCount > 0)
                        .ToList();

                tickets4 = tickets4
                        .OrderBy(x => x.Price)
                        .ToList();

                MinPrice = 9999999999999999999;

                //для вывода лучшего маршрута с двумя пересадками
                Ticket tick3 = null;
                //конечная точка
                Ticket t3 = null;

                foreach (var t in tickets2)
                {
                    //тут t2 используется как промежуточная точка
                    t2 = tickets4
                        .FirstOrDefault(x => t.Route.ArrivalCity == x.Route.DepartureCity
                                        && t.ArrivalTime.AddHours(1) <= x.DispatchTime);
                    if (t2 != null)
                    {
                        t3 = tickets3
                            .FirstOrDefault(x => t2.Route.ArrivalCity == x.Route.DepartureCity
                                            && t2.ArrivalTime.AddHours(1) <= x.DispatchTime);
                        if (t3 != null)
                        {
                            t1 = t;
                            if (MinPrice > t1.Price + t2.Price + t3.Price)
                            {
                                MinPrice = t1.Price + t2.Price + t3.Price;
                                tick1 = t1;
                                tick2 = t2;
                                tick3 = t3;
                            }
                        }
                    }
                }

                if (tick1 != null && tick2 != null && tick3 != null)
                {
                    Console.WriteLine("------------------------------------------");
                    Console.WriteLine("Лучшее предложение по цене с 2 пересадками");
                    PrintInfoOne(tick1);
                    Console.WriteLine();
                    PrintInfoOne(tick2);
                    Console.WriteLine();
                    PrintInfoOne(tick3);
                    Console.WriteLine();
                    Console.WriteLine($"Минимальная цена с 2 пересадками: {MinPrice}");
                }
            }

            if (option == 2)
            {
                List<City> city1 = new List<City>();

                foreach (var t in tickets)
                {
                    city1.Add(t.Route.DepartureCity);
                }

                city1.Distinct();

                Console.WriteLine("Введите точку отправки");
                foreach (var value in Enum.GetValues(typeof(City)))
                {
                    Console.WriteLine("{0}.  {1}",
                        (int)value, (value));
                }

                int option1 = TryParseInt();

                foreach (int i in city1)
                {
                    if (option1 != i)
                        continue;
                    else
                        tickets = tickets
                                .Where(t => t.Route.DepartureCity == (City)option1)
                                .ToList();
                }

                while (option1 > Enum.GetValues(typeof(City)).Length || option1 <= 0)
                {
                    Console.WriteLine("----------------------------------");
                    Console.WriteLine("Такой точки отправки не сущестует");
                    Console.WriteLine("Введите номер точки отправки снова");
                    foreach (var value in Enum.GetValues(typeof(City)))
                    {
                        Console.WriteLine("{0}.  {1}",
                           (int)value, (value));
                    }

                    option1 = TryParseInt();

                    foreach (int i in city1)
                    {
                        if (option1 != i)
                            continue;
                        else
                            tickets = tickets
                                    .Where(t => t.Route.DepartureCity == (City)option1)
                                    .ToList();
                    }
                }

                //лист билетов для пересадок
                var tickets2 = db.Tickets
                    .Include(t => t.Route)
                    .Where(t => t.DispatchTime > DateTime.Now
                         && t.TicketCount > 0
                         && t.Route.DepartureCity == (City)option1)
                    .ToList();

                if (tickets2 is null)
                {
                    Console.WriteLine("Таких билетов нет");
                    Console.WriteLine();
                    Console.WriteLine("-------------------------------------");
                    Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                    Console.ReadKey();
                    Console.Clear();
                    return;
                }

                List<City> city2 = new List<City>();

                foreach (var t in tickets)
                {
                    city2.Add(t.Route.ArrivalCity);
                }

                city2.Distinct();

                Console.WriteLine("Введите точку прибытия");
                foreach (var value in Enum.GetValues(typeof(City)))
                {
                    Console.WriteLine("{0}.  {1}",
                        (int)value, (value));
                }

                int option2 = TryParseInt();

                foreach (int i in city2)
                {
                    if (option2 != i)
                        continue;
                    else
                        tickets = tickets
                                .Where(t => t.Route.ArrivalCity == (City)option2)
                                .ToList();
                }

                while (option2 > Enum.GetValues(typeof(City)).Length || option2 <= 0)
                {
                    Console.WriteLine("----------------------------------");
                    Console.WriteLine("Такой точки прибытия не сущестует");
                    Console.WriteLine("Введите номер точки прибытия снова");
                    foreach (var value in Enum.GetValues(typeof(City)))
                    {
                        Console.WriteLine("{0}.  {1}",
                           (int)value, (value));
                    }

                    option2 = TryParseInt();

                    foreach (int i in city1)
                    {
                        if (option2 != i)
                            continue;
                        else
                            tickets = tickets
                                    .Where(t => t.Route.ArrivalCity == (City)option2)
                                    .ToList(); ;
                    }
                }

                //лист билетов для пересадок
                var tickets3 = db.Tickets
                                .Include(t => t.Route)
                                .Where(t => t.DispatchTime > DateTime.Now
                                        && t.TicketCount > 0
                                        && t.Route.ArrivalCity == (City)option2)
                                .ToList();

                if (tickets3 is null)
                {
                    Console.WriteLine("Таких билетов нет");
                    Console.WriteLine();
                    Console.WriteLine("-------------------------------------");
                    Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                    Console.ReadKey();
                    Console.Clear();
                    return;
                }

                if (tickets != null)
                {
                    tickets = tickets
                            .OrderBy(x => x.ArrivalTime - x.DispatchTime)
                            .ToList();
                    var t = tickets
                        .FirstOrDefault(x => x.ArrivalTime - x.DispatchTime > new TimeSpan(0, 0, 1));

                    Console.WriteLine("----------------------------------------");
                    Console.WriteLine("Лучшее предложение по времени без пересадок");
                    PrintInfoOne(t);
                }

                tickets2 = tickets2
                        .OrderBy(x => x.ArrivalTime - x.DispatchTime)
                        .ToList();
                tickets3 = tickets3
                        .OrderBy(x => x.ArrivalTime - x.DispatchTime)
                        .ToList();

                //для вывода лучшего маршрута с пересадками
                Ticket tick1 = null;
                Ticket tick2 = null;

                //точка отправки
                Ticket t1 = null;
                //точка прибытия
                Ticket t2 = null;

                TimeSpan MinHour = new TimeSpan(29, 23, 59, 59);

                foreach (var t in tickets2)
                {
                    t2 = tickets3
                        .FirstOrDefault(x => t.Route.ArrivalCity == x.Route.DepartureCity
                                             && t.ArrivalTime.AddHours(1) <= x.DispatchTime);
                    if (t2 != null)
                    {
                        t1 = t;
                        if (MinHour > (t2.ArrivalTime - t1.DispatchTime))
                        {
                            MinHour = (t2.ArrivalTime - t1.DispatchTime);
                            tick2 = t2;
                            tick1 = t1;
                        }
                    }
                }

                if (tick1 != null && tick2 != null)
                {
                    Console.WriteLine("--------------------------------------------");
                    Console.WriteLine("Лучшее предложение по времени с 1 пересадкой");
                    PrintInfoOne(tick1);
                    Console.WriteLine();
                    PrintInfoOne(tick2);
                }

                //промежуточный лист билетов для 2 пересадок
                var tickets4 = db.Tickets
                    .Where(x => x.DispatchTime > DateTime.Now
                                && x.TicketCount > 0)
                    .ToList();

                tickets4 = tickets4
                        .OrderBy(x => x.ArrivalTime - x.DispatchTime)
                        .ToList();

                MinHour = new TimeSpan(29, 23, 59, 59);

                //для вывода лучшего маршрута с двумя пересадками
                Ticket tick3 = null;
                //конечная точка
                Ticket t3 = null;

                foreach (var t in tickets2)
                {
                    //тут t2 используется для обозначения промежуточной точки
                    t2 = tickets4
                        .FirstOrDefault(x => t.Route.ArrivalCity == x.Route.DepartureCity
                                        && t.ArrivalTime.AddHours(1) <= x.DispatchTime);
                    if (t2 != null)
                    {
                        t3 = tickets3
                            .FirstOrDefault(x => t2.Route.ArrivalCity == x.Route.DepartureCity
                                            && t2.ArrivalTime.AddHours(1) <= x.DispatchTime);
                        if (t3 != null)
                        {
                            t1 = t;
                            if (MinHour > (t3.ArrivalTime - t1.DispatchTime))
                            {
                                MinHour = (t3.ArrivalTime - t1.DispatchTime);
                                tick1 = t1;
                                tick2 = t2;
                                tick3 = t3;
                            }
                        }
                    }
                }

                if (tick1 != null && tick2 != null && tick3 != null)
                {
                    Console.WriteLine("---------------------------------------------");
                    Console.WriteLine("Лучшее предложение по времени с 2 пересадками");
                    PrintInfoOne(tick1);
                    Console.WriteLine();
                    PrintInfoOne(tick2);
                    Console.WriteLine();
                    PrintInfoOne(tick3);
                }
            }

            Console.WriteLine();
            Console.WriteLine("-------------------------------------");
            Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
            Console.ReadKey();
            Console.Clear();
        }

        /// <summary>
        /// вывод информации о одном билете
        /// </summary>
        /// <param name="ticket"></param>
        public static void PrintInfoOne(Ticket ticket)
        {
            Console.WriteLine($"ID билета: {ticket.ID}");
            Console.WriteLine($"ID пути: {ticket.RouteID}");
            Console.WriteLine($"Тип транспорта: {ticket.Route.TransportType}");
            Console.WriteLine($"Точка отправки: {ticket.Route.DepartureCity}");
            Console.WriteLine($"Точка прибытия: {ticket.Route.ArrivalCity}");
            Console.WriteLine($"Время отправки: {ticket.DispatchTime}");
            Console.WriteLine($"Время прибытия: {ticket.ArrivalTime}");
            Console.WriteLine($"Цена билета: {ticket.Price}");
            Console.WriteLine($"Количество билетов: {ticket.TicketCount}");
        }

        /// <summary>
        /// вывод информации о списке билетов
        /// </summary>
        /// <param name="tickets"></param>
        public static void PrintInfoList(List<Ticket> tickets)
        {
            foreach (var t in tickets)
            {
                Console.WriteLine("---------------------------");
                Console.WriteLine($"ID билета: {t.ID}");
                Console.WriteLine($"ID пути: {t.RouteID}");
                Console.WriteLine($"Тип транспорта: {t.Route.TransportType}");
                Console.WriteLine($"Точка отправки: {t.Route.DepartureCity}");
                Console.WriteLine($"Точка прибытия: {t.Route.ArrivalCity}");
                Console.WriteLine($"Цена билета: {t.Price}");
                Console.WriteLine($"Время отправки: {t.DispatchTime}");
                Console.WriteLine($"Время прибытия: {t.ArrivalTime}");
                Console.WriteLine($"Количество билетов: {t.TicketCount}");
            }
        }

        /// <summary>
        /// проверка на возможность перевести string в int
        /// </summary>
        /// <returns></returns>
        public static int TryParseInt()
        {
            int i;
            bool parse = int.TryParse(Console.ReadLine(), out i);
            while (parse == false)
            {
                Console.WriteLine("----------------------------");
                Console.WriteLine("Неверный ввод. Введите число");
                parse = int.TryParse(Console.ReadLine(), out i);
            }
            return i;
        }

        /// <summary>
        /// проверка на возможность перевести string в datetime
        /// </summary>
        /// <returns></returns>
        public static DateTime TryParseDateTime()
        {
            DateTime i;
            bool parse = DateTime.TryParse(Console.ReadLine(), out i);
            while (parse == false)
            {
                Console.WriteLine("----------------------------------------------");
                Console.WriteLine("Неверный ввод. Введите дату в формате гггг-мм-дд");
                parse = DateTime.TryParse(Console.ReadLine(), out i);
            }
            return i;
        }
    }
}
