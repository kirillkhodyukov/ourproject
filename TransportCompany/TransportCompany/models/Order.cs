﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using TransportCompany.enums;
using TransportCompany.interfces;

namespace TransportCompany.models
{
    public class Order : IEntity<int>
    {
        public int ID { get; set; } 

        /// <summary>
        /// id билета
        /// </summary>
        [Required]
        [ForeignKey("Ticket")]
        public int TicketID { get; set; }

        /// <summary>
        /// id пользователя
        /// </summary>
        [Required]
        [ForeignKey("User")]
        public int UserID { get; set; }

        /// <summary>
        /// дата покупки билета
        /// </summary>
        public DateTime DateOfOrder
        {
            get
            {
                return DateTime.Now;
            }
            set { }
        }

        /// <summary>
        /// число билетов заказа
        /// </summary>
        [Required]
        public int CountOfTicket { get; set; }

        /// <summary>
        /// общая стоимость билетов
        /// </summary>
        [Required]
        public decimal TotalPrice { get; set; }

        /// <summary>
        /// статус заказа
        /// </summary>
        [Required]
        public OrderStatus OrderStatus { get; set; }

        /// <summary>
        /// навигационные свойства
        /// </summary>
        public virtual Ticket Ticket { get; set; }
        public virtual User User { get; set; }



        /// <summary>
        /// вывод информации покупок
        /// </summary>
        /// <param name="db"></param>
        /// <param name="id">передаётся ID текущего пользователя</param>
        public static void PrintOrderInfo(ApplicationDbContext db, int id)
        {
            Console.Clear();

            var acc = db.Accounts.FirstOrDefault(x => x.ID == id
                                                   && x.IsAdmin == true);

            List<Order> orders = null;

            if (acc != null)
            {
                orders = db.Orders
                    .Include(x => x.Ticket)
                    .ToList();
            }
            else
            {
                orders = db.Orders
                    .Include(x => x.Ticket)
                    .Where(x => x.UserID == id)
                    .ToList();
            }

            if (orders is null)
            {
                Console.WriteLine("Нет заказов");
                Console.WriteLine();
                Console.WriteLine("-------------------------------------");
                Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                Console.ReadKey();
                Console.Clear();
                return;
            }

            foreach (var o in orders)
            {
                if (o.Ticket.DispatchTime < DateTime.Now)
                {
                    o.OrderStatus = OrderStatus.Сlosed;
                }
            }

            db.UpdateRange(orders);
            db.SaveChanges();


            Console.WriteLine("Выберите какие заказы выводить");
            Console.WriteLine("1. Все");
            Console.WriteLine("2. Действующие");

            int option = TryParseInt();

            while (option != 1 && option != 2)
            {
                Console.WriteLine("-------------------------------");
                Console.WriteLine("Некорректный ввод");
                Console.WriteLine("Выберите какие заказы выводить");
                Console.WriteLine("1. Все");
                Console.WriteLine("2. Действующие");
            }

            if (option == 1)
            {
                foreach (var o in orders)
                {
                    Console.WriteLine("---------------------------------");
                    Console.WriteLine($"ID заказа: {o.ID}");
                    Console.WriteLine($"ID билета: {o.TicketID}");
                    Console.WriteLine($"Количество билетов: {o.CountOfTicket}");
                    Console.WriteLine($"Цена заказа: {o.TotalPrice}");
                    Console.WriteLine($"Статус заказа: {o.OrderStatus}");
                }
            }
            else
            {
                orders = orders
                        .Where(x => x.OrderStatus != OrderStatus.Сlosed
                                && x.OrderStatus != OrderStatus.Returned)
                        .ToList();

                foreach (var o in orders)
                {
                    Console.WriteLine("---------------------------------");
                    Console.WriteLine($"ID заказа: {o.ID}");
                    Console.WriteLine($"ID билета: {o.TicketID}");
                    Console.WriteLine($"Количество билетов: {o.CountOfTicket}");
                    Console.WriteLine($"Цена заказа: {o.TotalPrice}");
                    Console.WriteLine($"Статус заказа: {o.OrderStatus}");
                }
            }

            Console.WriteLine();
            Console.WriteLine("-------------------------------------");
            Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
            Console.ReadKey();
            Console.Clear();
        }

        /// <summary>
        /// метод возврата билетов
        /// </summary>
        /// <param name="db"></param>
        /// <param name="id">передаётся id текущего пользователя</param>
        public static void СancellationOfReservation(ApplicationDbContext db, int id)
        {
            Console.Clear();

            var or = db.Orders
                .Include(x => x.Ticket)
                .Where(x => x.UserID == id)
                .ToList();

            if (or != null)
            {
                foreach (var o in or)
                {
                    if (o.Ticket.DispatchTime < DateTime.Now)
                    {
                        o.OrderStatus = OrderStatus.Сlosed;
                    }
                }

                db.UpdateRange(or);
                db.SaveChanges();
            }

            or = or
                .Where(x => (x.OrderStatus == OrderStatus.Reserved
                                || x.OrderStatus == OrderStatus.Bought)
                                && x.Ticket.DispatchTime > DateTime.Now.AddDays(1))
                .ToList();

            if (or is null)
            {
                Console.WriteLine("У пользователя нет заказов");
                Console.WriteLine();
                Console.WriteLine("-------------------------------------");
                Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                Console.ReadKey();
                Console.Clear();
                return;
            }

            foreach(var o in or)
            {
                Console.WriteLine("-----------------------------------");
                Console.WriteLine($"ID заказа: {o.ID}");
                Console.WriteLine($"ID билета: {o.TicketID}");
                Console.WriteLine($"Дата заказа: {o.DateOfOrder}");
                Console.WriteLine($"Количество билетов: {o.CountOfTicket}");
                Console.WriteLine($"Общая цена заказа: {o.TotalPrice}");
            }
            Console.WriteLine();

            Console.WriteLine("Введите ID покупки");

            //локальная переменная int(будет изменяться с каждым вводом)
            int input = TryParseInt();

            Order order = or.FirstOrDefault(x => x.ID == input);

            if (order is null)
            {
                Console.WriteLine("Покупки с таким ID не существует");
                Console.WriteLine();
                Console.WriteLine("-------------------------------------");
                Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                Console.ReadKey();
                Console.Clear();
                return;
            }

            Ticket ticket = db.Tickets.FirstOrDefault(x => x.ID == order.TicketID);

            ticket.TicketCount += order.CountOfTicket;

            order.OrderStatus = OrderStatus.Returned;

            db.Update(ticket);
            db.Update(order);
            db.SaveChanges();

            Console.WriteLine();
            Console.WriteLine("Билеты возвращены");
            Console.WriteLine();
            Console.WriteLine("-------------------------------------");
            Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
            Console.ReadKey();
            Console.Clear();
        }

        /// <summary>
        /// проверка на возможность перевести string в int
        /// </summary>
        /// <returns></returns>
        public static int TryParseInt()
        {
            int i;
            bool parse = int.TryParse(Console.ReadLine(), out i);
            while (parse == false)
            {
                Console.WriteLine("----------------------------");
                Console.WriteLine("Неверный ввод. Введите число");
                parse = int.TryParse(Console.ReadLine(), out i);
            }
            return i;
        }
    }
}
