﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using TransportCompany.enums;
using TransportCompany.interfces;

namespace TransportCompany.models
{
    public class Route : IEntity<int>
    {
        public int ID { get; set; }

        /// <summary>
        /// тип транспорта в билете
        /// </summary>
        [Required]
        public TransportType TransportType { get; set; }

        /// <summary>
        /// точка отправки
        /// </summary>
        [Required]
        public City DepartureCity { get; set; }

        /// <summary>
        /// точка прибытия
        /// </summary>
        [Required]
        public City ArrivalCity { get; set; }



        /// <summary>
        /// Информация о существующих путях для админов
        /// </summary>
        public static void RoutesInfo(ApplicationDbContext db)
        {
            Console.Clear();

            var routes = db.Routes.ToList();

            Console.WriteLine("Информация о существующих путях");
            foreach (var r in routes)
            {
                Console.WriteLine("-----------------------------");
                Console.WriteLine($"ID маршрута: {r.ID}");
                Console.WriteLine($"Тип транспота: {r.TransportType}");
                Console.WriteLine($"Точка отправки: {r.DepartureCity}");
                Console.WriteLine($"Точка прибытия: {r.ArrivalCity}");
            }

            Console.WriteLine();
            Console.WriteLine("-------------------------------------");
            Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
            Console.ReadKey();
            Console.Clear();
        }

        /// <summary>
        /// метод для добавления пути
        /// </summary>
        public static void AddRoute(ApplicationDbContext db)
        {
            Console.Clear();

            using (var transaction = db.Database.BeginTransaction())
            {
                Route route = new Route();
                Console.WriteLine("Обязательные поля обозначаются как: ^");

                Console.WriteLine("Введите номер типа транспорта^");
                foreach (var value in Enum.GetValues(typeof(TransportType)))
                {
                    Console.WriteLine("{0}.  {1}",
                       (int)value, (value));
                }

                int tType = TryParseInt();

                foreach (int i in Enum.GetValues(typeof(TransportType)))
                {
                    if (tType != i)
                        continue;
                    else
                        route.TransportType = (TransportType)tType;
                }

                while (route.TransportType == 0)
                {
                    Console.WriteLine("------------------------------");
                    Console.WriteLine("Такого транспорта не сущестует");
                    Console.WriteLine("Введите номер типа транспорта снова");
                    foreach (var value in Enum.GetValues(typeof(TransportType)))
                    {
                        Console.WriteLine("{0}.  {1}",
                           (int)value, (value));
                    }

                    tType = TryParseInt();

                    foreach (int i in Enum.GetValues(typeof(TransportType)))
                    {
                        if (tType != i)
                            continue;
                        else
                            route.TransportType = (TransportType)tType;
                    }
                }

                Console.WriteLine("------------------------------");
                Console.WriteLine("Введите номер точки отправки^");
                foreach (var value in Enum.GetValues(typeof(City)))
                {
                    Console.WriteLine("{0}.  {1}",
                       (int)value, (value));
                }

                int DepartureCity = TryParseInt();

                foreach (int i in Enum.GetValues(typeof(City)))
                {
                    if (DepartureCity != i)
                        continue;
                    else
                        route.DepartureCity = (City)DepartureCity;
                }

                while (route.DepartureCity == 0)
                {
                    Console.WriteLine("------------------------------");
                    Console.WriteLine("Такого города не сущестует");
                    Console.WriteLine("Введите номер города отправки снова");
                    foreach (var value in Enum.GetValues(typeof(City)))
                    {
                        Console.WriteLine("{0}.  {1}",
                           (int)value, (value));
                    }
                    DepartureCity = TryParseInt();
                    foreach (int i in Enum.GetValues(typeof(City)))
                    {
                        if (DepartureCity != i)
                            continue;
                        else
                            route.DepartureCity = (City)DepartureCity;
                    }
                }

                Console.WriteLine("------------------------------");
                Console.WriteLine("Введите номер точки прибытия^");
                foreach (var value in Enum.GetValues(typeof(City)))
                {
                    Console.WriteLine("{0}.  {1}",
                       (int)value, (value));
                }

                int ArrivalCity = TryParseInt();

                foreach (int i in Enum.GetValues(typeof(City)))
                {
                    if (ArrivalCity != i)
                        continue;
                    else
                        route.ArrivalCity = (City)ArrivalCity;
                }

                while (route.ArrivalCity == 0 || route.ArrivalCity == route.DepartureCity)
                {
                    if (route.ArrivalCity == 0)
                    {
                        Console.WriteLine("------------------------------");
                        Console.WriteLine("Такого города не сущестует");
                        Console.WriteLine("Введите номер города прибытия снова");
                        foreach (var value in Enum.GetValues(typeof(City)))
                        {
                            Console.WriteLine("{0}.  {1}",
                               (int)value, (value));
                        }

                        ArrivalCity = TryParseInt();

                        foreach (int i in Enum.GetValues(typeof(City)))
                        {
                            if (ArrivalCity != i)
                                continue;
                            else
                                route.ArrivalCity = (City)ArrivalCity;
                        }
                    }

                    if (route.ArrivalCity == route.DepartureCity)
                    {
                        Console.WriteLine("-----------------------------------------------------------");
                        Console.WriteLine("Город прибытия не может быть таким же, как и город отправки");
                        Console.WriteLine("Введите номер города прибытия снова");
                        foreach (var value in Enum.GetValues(typeof(City)))
                        {
                            Console.WriteLine("{0}.  {1}",
                               (int)value, (value));
                        }

                        ArrivalCity = TryParseInt();

                        foreach (int i in Enum.GetValues(typeof(City)))
                        {
                            if (ArrivalCity != i)
                                continue;
                            else
                                route.ArrivalCity = (City)ArrivalCity;
                        }
                    }
                }

                db.Add(route);
                db.SaveChanges();

                transaction.Commit();

                Console.WriteLine("Новый путь зарегестрирован");
                Console.WriteLine();
                Console.WriteLine("-------------------------------------");
                Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                Console.ReadKey();
                Console.Clear();
            }
        }

        /// <summary>
        /// проверка на возможность перевести string в int
        /// </summary>
        /// <returns></returns>
        public static int TryParseInt()
        {
            int i;
            bool parse = int.TryParse(Console.ReadLine(), out i);
            while (parse == false)
            {
                Console.WriteLine("----------------------------");
                Console.WriteLine("Неверный ввод. Введите число");
                parse = int.TryParse(Console.ReadLine(), out i);
            }
            return i;
        }
    }
}
