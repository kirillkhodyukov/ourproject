﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using TransportCompany.interfces;

namespace TransportCompany.models
{
    public class Account : IEntity<int>
    {
        public int ID { get; set; }

        /// <summary>
        /// логин аккаунта
        /// </summary>
        [Required]
        [MinLength(4)]
        public string Login { get; set; }

        /// <summary>
        /// пароль аккаунта
        /// </summary>
        [Required]
        [MinLength(4)]
        public string Password { get; set; }

        /// <summary>
        /// мэйл аккаунта
        /// </summary>
        [Required]
        [MinLength(4)]
        public string Email { get; set; }

        /// <summary>
        /// дата создания аккаута
        /// </summary>
        public DateTime DateOfCreate
        {
            get
            {
                return DateTime.Now;
            }
            set { }
        }

        /// <summary>
        /// является ли админом или нет
        /// (true = admin \ false = user)
        /// </summary>
        [Required]
        public bool IsAdmin { get; set; }



        public static void AddAdminAcc(ApplicationDbContext db)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                Account admin = new Account();

                admin.IsAdmin = true;

                Console.WriteLine("Уникальные поля обозначаются как: *");
                Console.WriteLine("Обязательные поля обозначаются как: ^");
                Console.WriteLine("-------------------------------------");
                Console.WriteLine("Введите логин*^");
                Console.WriteLine("Минимальное количество символов 4");
                admin.Login = Console.ReadLine();

                Account a = db.Accounts.FirstOrDefault(x => x.Login == admin.Login);
                if (a != null)
                {
                    Console.WriteLine("Админ с таким логином уже существует");
                    Console.WriteLine();
                    Console.WriteLine("-------------------------------------");
                    Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                    Console.ReadKey();
                    Console.Clear();
                    return;
                }

                admin.Login = admin.Login.Trim();

                while (admin.Login.Length < 4)
                {
                    Console.WriteLine("----------------------------------");
                    Console.WriteLine("Минимальное количество символов 4");
                    Console.WriteLine("Введите логин повторно");
                    admin.Login = Console.ReadLine();

                    a = db.Accounts.FirstOrDefault(x => x.Login == admin.Login);
                    if (a != null)
                    {
                        Console.WriteLine("Админ с таким логином уже существует");
                        Console.WriteLine();
                        Console.WriteLine("-------------------------------------");
                        Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                        Console.ReadKey();
                        Console.Clear();
                        return;
                    }

                    admin.Login = admin.Login.Trim();
                }

                Console.WriteLine("----------------------------------");
                Console.WriteLine("Введите пароль*^");
                Console.WriteLine("Минимальное количество символов 4");

                admin.Password = Console.ReadLine();

                while (admin.Password.Length < 4)
                {
                    Console.WriteLine("----------------------------------");
                    Console.WriteLine("Минимальное количество символов 4");
                    Console.WriteLine("Введите пароль повторно");
                    admin.Password = Console.ReadLine();
                }

                Console.WriteLine("----------------------------------");
                Console.WriteLine("Введите свою почту*^");
                Console.WriteLine("Минимальное количество символов 4");

                admin.Email = Console.ReadLine();

                a = db.Users.FirstOrDefault(x => x.Email == admin.Email);
                if (a != null)
                {
                    Console.WriteLine("Пользователь с такой почтой уже существует");
                    Console.WriteLine();
                    Console.WriteLine("-------------------------------------");
                    Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                    Console.ReadKey();
                    Console.Clear();
                    return;
                }

                admin.Email = admin.Email.Trim();

                while (admin.Email.Length < 4)
                {
                    Console.WriteLine("----------------------------------");
                    Console.WriteLine("Минимальное количество символов 4");
                    Console.WriteLine("Введите почту повторно");
                    admin.Email = Console.ReadLine();

                    a = db.Users.FirstOrDefault(x => x.Email == admin.Email);
                    if (a != null)
                    {
                        Console.WriteLine("Пользователь с такой почтой уже существует");
                        Console.WriteLine();
                        Console.WriteLine("-------------------------------------");
                        Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                        Console.ReadKey();
                        Console.Clear();
                        return;
                    }

                    admin.Email = admin.Email.Trim();
                }

                db.Add(admin);
                db.SaveChanges();

                transaction.Commit();

                Console.WriteLine("Пользователь добавлен");
                Console.WriteLine();
                Console.WriteLine("-------------------------------------");
                Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
                Console.ReadKey();
                Console.Clear();
            }
        }
    }
}
