﻿using System;
using System.Linq;
using TransportCompany.enums;
using TransportCompany.models;

namespace TransportCompany
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                try
                {
                    SeedBaseAdminAcc(db);
                }
                catch
                {
                    Console.WriteLine("Ошибка при добавлении администратора. В базе данных нет администратора");
                    return;
                }

                int input = 0;
                Console.WriteLine("Добро пожаловать");
                while (input != 3)
                {
                    Console.WriteLine("Выберите доступное действие:");
                    Console.WriteLine("1. Войти в аккаунт");
                    Console.WriteLine("2. Создать в аккаунт");
                    Console.WriteLine("3. Выйти из приложения");
                    Console.WriteLine();
                    input = TryParseInt();

                    while (input < 1 || input > 3)
                    {
                        Console.WriteLine("----------------------------");
                        Console.WriteLine("Некорректный ввод");
                        Console.WriteLine("Выберите доступное действие:");
                        Console.WriteLine("1. Войти в аккаунт");
                        Console.WriteLine("2. Создать в аккаунт");
                        Console.WriteLine("3. Выйти из приложения");
                        Console.WriteLine();
                        input = TryParseInt();
                    }

                    switch (input)
                    {
                        case 1:

                            Console.Clear();
                            Console.WriteLine("Введите свой логин аккаунта:");
                            string logAcc = Console.ReadLine();
                            Console.WriteLine("Введите свой пароль аккаунта:");
                            string passwordAcc = Console.ReadLine();

                            var admin = db.Accounts.FirstOrDefault(x => x.Login == logAcc
                                                                && x.Password == passwordAcc);

                            var user = db.Users.FirstOrDefault(x => x.Login == logAcc
                                                                && x.Password == passwordAcc);

                            if (admin == null && user == null)
                            {
                                Console.WriteLine("Такого пользователя не существует");
                                Console.WriteLine();
                                Console.WriteLine("------------------------------------");
                                Console.WriteLine("Нажмите любую кнопку для продолжения");
                                Console.ReadKey();
                                Console.Clear();
                            }

                            if (admin != null && admin.IsAdmin == true)
                            {
                                int option = 0;
                                while (option != 11)
                                {
                                    Console.Clear();
                                    Console.WriteLine("Выберите доступное действие:");
                                    Console.WriteLine("1. Смотреть список доступных маршрутов");
                                    Console.WriteLine("2. Добавить возможные маршруты");
                                    Console.WriteLine("3. Смотреть список билетов");
                                    Console.WriteLine("4. Смотреть список билетов c фильтрами");
                                    Console.WriteLine("5. Добавить возможные к покупке билеты");
                                    Console.WriteLine("6. Смотреть список заказов пользователей");
                                    Console.WriteLine("7. Смотреть список городов");
                                    Console.WriteLine("8. Смотреть список типов транспорта");
                                    Console.WriteLine("9. Смотреть список статусов заказа");
                                    Console.WriteLine("10. Добавить админа");
                                    Console.WriteLine("11. Выйти из аккаунта");
                                    Console.WriteLine();
                                    option = TryParseInt();

                                    while (option < 1 || option > 11)
                                    {
                                        Console.WriteLine("----------------------------");
                                        Console.WriteLine("Некорректный ввод");
                                        Console.WriteLine("Выберите доступное действие:");
                                        Console.WriteLine("1. Смотреть список доступных маршрутов");
                                        Console.WriteLine("2. Добавить возможные маршруты");
                                        Console.WriteLine("3. Смотреть список билетов");
                                        Console.WriteLine("4. Смотреть список билетов c фильтрами");
                                        Console.WriteLine("5. Добавить возможные к покупке билеты");
                                        Console.WriteLine("6. Смотреть список заказов пользователей");
                                        Console.WriteLine("7. Смотреть список городов");
                                        Console.WriteLine("8. Смотреть список типов транспорта");
                                        Console.WriteLine("9. Смотреть список статусов заказа");
                                        Console.WriteLine("10. Добавить админа");
                                        Console.WriteLine("11. Выйти из аккаунта");
                                        Console.WriteLine();
                                        option = TryParseInt();
                                    }

                                    switch (option)
                                    {
                                        case 1:
                                            Route.RoutesInfo(db);
                                            break;

                                        case 2:
                                            Route.AddRoute(db);
                                            break;

                                        case 3:
                                            Ticket.TicketInfoForAcc(db, admin.ID);
                                            break;

                                        case 4:
                                            Ticket.TicketInfoForAccWithFilters(db, admin.ID);
                                            break;

                                        case 5:
                                            Ticket.AddTicket(db);
                                            break;

                                        case 6:
                                            Order.PrintOrderInfo(db, admin.ID);
                                            break;

                                        case 7:
                                            CityInfo();
                                            break;

                                        case 8:
                                            TransportTypeInfo();
                                            break;

                                        case 9:
                                            OrderStatusInfo();
                                            break;

                                        case 10:
                                            Account.AddAdminAcc(db);
                                            break;

                                        default:
                                            break;
                                    }
                                }
                            }

                            if (user != null)
                            {
                                int option = 0;
                                while (option != 8)
                                {
                                    Console.Clear();
                                    Console.WriteLine("Выберите доступное действие:");
                                    Console.WriteLine("1. Посмотреть весь список доступных билетов");
                                    Console.WriteLine("2. Посмотреть весь список доступных билетов c фильтрами");
                                    Console.WriteLine("3. Найти лучший маршрут(по цене/по времени)");
                                    Console.WriteLine("4. Купить билет(ы)");
                                    Console.WriteLine("5. Забронировать билеты(ы)");
                                    Console.WriteLine("6. Посмотреть все свои заказы");
                                    Console.WriteLine("7. Отменить заказ");
                                    Console.WriteLine("8. Выйти из аккаунта");
                                    option = TryParseInt();

                                    while (option < 1 || option > 8)
                                    {
                                        Console.WriteLine("----------------------------");
                                        Console.WriteLine("Некорректный ввод");
                                        Console.WriteLine("Выберите доступное действие:");
                                        Console.WriteLine("1. Посмотреть весь список доступных билетов");
                                        Console.WriteLine("2. Посмотреть весь список доступных билетов c фильтрами");
                                        Console.WriteLine("3. Найти лучший маршрут(по цене/по времени)");
                                        Console.WriteLine("4. Купить билет(ы)");
                                        Console.WriteLine("5. Забронировать билеты(ы)");
                                        Console.WriteLine("6. Посмотреть все свои заказы");
                                        Console.WriteLine("7. Отменить заказ");
                                        Console.WriteLine("8. Выйти из аккаунта");
                                        option = TryParseInt();
                                    }

                                    switch (option)
                                    {
                                        case 1:
                                            Ticket.TicketInfoForAcc(db, user.ID);
                                            break;

                                        case 2:
                                            Ticket.TicketInfoForAccWithFilters(db, user.ID);
                                            break;

                                        case 3:
                                            Ticket.BestRoute(db);
                                            break;

                                        case 4:
                                            Ticket.ByeTicket(db, user.ID);
                                            break;

                                        case 5:
                                            Ticket.TicketReservation(db, user.ID);
                                            break;

                                        case 6:
                                            Order.PrintOrderInfo(db, user.ID);
                                            break;

                                        case 7:
                                            Order.СancellationOfReservation(db, user.ID);
                                            break;

                                        default:
                                            break;
                                    }
                                }
                            }

                            break;

                        case 2:
                            User.AddUserAccount(db);

                            break;
                    }

                    Console.Clear();
                }

                Console.WriteLine();
                Console.WriteLine("Спасибо за альфа-тест. Всего доброго.");
            }
        }



        /// <summary>
        /// добавление дефолтного админа
        /// </summary>
        /// <param name="db"></param>
        public static void SeedBaseAdminAcc(ApplicationDbContext db)
        {
            Account acc = db.Accounts.FirstOrDefault(x => x.IsAdmin == true);

            if (acc is null)
            {
                acc = new Account
                {
                    IsAdmin = true,
                    Login = "admin",
                    Password = "1234",
                    Email = "@gmail.com"
                };
            
                db.Add(acc);
                db.SaveChanges();
            }
        }
        
        /// <summary>
        /// города в списке enum
        /// </summary>
        public static void CityInfo()
        {
            Console.Clear();

            Console.WriteLine("Доступные города");
            foreach (var value in Enum.GetValues(typeof(City)))
            {
                Console.WriteLine("{0}.  {1}",
                   (int)value, (value));
            }
            Console.WriteLine();
            Console.WriteLine("-------------------------------------");
            Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
            Console.ReadKey();
            Console.Clear();
        }

        /// <summary>
        /// тип транспорта в списке enum
        /// </summary>
        public static void TransportTypeInfo()
        {
            Console.Clear();

            Console.WriteLine("Доступные типы транспорта");
            foreach (var value in Enum.GetValues(typeof(TransportType)))
            {
                Console.WriteLine("{0}.  {1}",
                   (int)value, (value));
            }
            Console.WriteLine();
            Console.WriteLine("-------------------------------------");
            Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
            Console.ReadKey();
            Console.Clear();
        }

        /// <summary>
        /// тип транспорта в списке enum
        /// </summary>
        public static void OrderStatusInfo()
        {
            Console.Clear();

            Console.WriteLine("Доступные статусы заказов");
            foreach (var value in Enum.GetValues(typeof(OrderStatus)))
            {
                Console.WriteLine("{0}.  {1}",
                   (int)value, (value));
            }
            Console.WriteLine();
            Console.WriteLine("-------------------------------------");
            Console.WriteLine("Нажмите любую кнопку чтобы продолжить");
            Console.ReadKey();
            Console.Clear();
        }

        /// <summary>
        /// проверка на возможность перевести string в int
        /// </summary>
        /// <returns></returns>
        public static int TryParseInt()
        {
            int i;
            bool parse = int.TryParse(Console.ReadLine(), out i);
            while (parse == false)
            {
                Console.WriteLine("----------------------------");
                Console.WriteLine("Неверный ввод. Введите число");
                parse = int.TryParse(Console.ReadLine(), out i);
            }
            return i;
        }
    }
}