﻿namespace TransportCompany.enums
{
    /// <summary>
    /// города отправки и прибытия
    /// </summary>
    public enum City
    {
        /// <summary>
        /// New-York
        /// </summary>
        NY = 1,

        /// <summary>
        /// Бишкек
        /// </summary>
        Bishkek = 2,

        /// <summary>
        /// Монако
        /// </summary>
        Monako = 3,

        /// <summary>
        /// Париж
        /// </summary>
        Paris = 4,

        /// <summary>
        /// Пекин
        /// </summary>
        Beijing = 5
    }
}
