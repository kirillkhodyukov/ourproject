﻿namespace TransportCompany.enums
{
    /// <summary>
    /// статус заказа
    /// </summary>
    public enum OrderStatus
    {
        /// <summary>
        /// зарезервированный
        /// </summary>
        Reserved = 1,

        /// <summary>
        /// купленный
        /// </summary>
        Bought = 2,

        /// <summary>
        /// возвращённый
        /// </summary>
        Returned = 3,

        /// <summary>
        /// закрытый
        /// </summary>
        Сlosed = 4
    }
}
