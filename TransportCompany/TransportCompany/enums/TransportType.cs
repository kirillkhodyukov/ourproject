﻿namespace TransportCompany.enums
{
    /// <summary>
    /// тип транспорта
    /// </summary>
    public enum TransportType
    {
        /// <summary>
        /// самолёт
        /// </summary>
        Airplane = 1,

        /// <summary>
        /// поезд
        /// </summary>
        Train = 2,

        /// <summary>
        /// машина
        /// </summary>
        Car = 3,

        /// <summary>
        /// корабль
        /// </summary>
        Ship = 4
    }
}
