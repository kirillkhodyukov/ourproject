﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using TransportCompany.models;

namespace TransportCompany
{
    public class ApplicationDbContext : DbContext
    { 
        public DbSet<Account> Accounts { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Route> Routes { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Order> Orders { get; set; }

        /// <summary>
        /// условия при создании моделей
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>()
            .HasIndex(x => x.Login)
            .IsUnique();
            modelBuilder.Entity<Account>()
            .HasIndex(x => x.Email)
            .IsUnique();
        }

        /// <summary>
        /// Подключение к базе
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(@"C:\Users\User\Desktop\с# 4 модуль\ourproject\TransportCompany\TransportCompany");
            builder.AddJsonFile("appsettings.json");
            var config = builder.Build();

            var connectionString = config.GetConnectionString("DefaultConnection");
            optionsBuilder.UseSqlServer(connectionString);
        }
    }
}
