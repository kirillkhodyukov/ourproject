﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationDbContext.enums
{
    public enum OrderStatus
    {
        Reserved = 1,
        Bought = 2,
        Returned = 3,
        Closed = 4
    }
}
