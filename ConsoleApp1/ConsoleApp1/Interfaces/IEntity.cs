﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationDbContext.Interfaces
{
    public interface IEntity<T>
    {
        public T ID { get; set; }
    }
}
