﻿using ApplicationDbContext.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ApplicationDbContext.models
{
    public abstract class Account : IEntity<int> //Класс аккаунт наследуется от класса IEntity для получения ID
    {
        public int ID { get; set; } // id аккаунта интом
        [Required, MinLength(4)]
        public string Login { get; set; } // login аккаунта строкой
        [Required, MinLength(4)]
        public string Password { get; set; }// пароль строкой
        [Required, MinLength(4)]
        public string Email { get; set; }  // почта строкой
        public DateTime DateOfCreate { get { return DateTime.Now; } set { } } // дата и время создания аккаунта
    }
}
