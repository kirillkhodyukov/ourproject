﻿using ApplicationDbContext.enums;
using ApplicationDbContext.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ApplicationDbContext.models
{
    public class Route : IEntity<int> //Route наследует IEntity для получения Id
    {
        public int ID { get; set; } // Id маршрута
        [Required]
        public TransportType TransportType { get; set; } // Вид транспорта, используемый для прохождения маршрута
        public City DepartureCity { get; set; } // Точка посадки
        public City ArrivalCity { get; set; }// Точка высадки
        public static void AddRoute(ApplicationDbContext db)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                Route route = new Route();
                int tType = int.Parse(Console.ReadLine());
                route.TransportType = (TransportType)tType;
                int dCity = int.Parse(Console.ReadLine());
                route.DepartureCity = (City)dCity;
                int aCity = int.Parse(Console.ReadLine());
                route.ArrivalCity = (City)aCity;
                db.Add(route);
                db.SaveChanges();
                transaction.Commit();
            }
        }
    }
}
