﻿using ApplicationDbContext.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace ApplicationDbContext.models
{
    public class Ticket : IEntity<int> // Ticket наследует IEntity для получения Id
    {
        public int ID { get; set; }//Id билеета
        public DateTime DateOfOrder { get { return DateTime.Now; } set { } }
        [Required]
        public decimal Price { get; set; } // цена билета
        [Required, ForeignKey("Route")]
        public int RouteID { get; set; } // ш
        public virtual Route Route { get; set; }
        [Required]
        public DateTime DispatchTime { get; set; }
        [Required]
        public DateTime ArrivalTime { get; set; }
        [Required]
        public int TicketCount { get; set; }
        public static void AddTicket(ApplicationDbContext db)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                Ticket ticket = new Ticket();
                int id = int.Parse(Console.ReadLine());
                var route = db.Routes.FirstOrDefault(x => x.ID == id);
                if (route is null)
                {
                    Console.WriteLine("некорректный ввод");
                    return;
                }
                ticket.RouteID = route.ID;
                ticket.Price = int.Parse(Console.ReadLine());
                if (ticket.Price < 0)
                {
                    Console.WriteLine("некорректный ввод");
                    return;
                }
                ticket.DispatchTime = DateTime.Parse(Console.ReadLine());
                int hours = int.Parse(Console.ReadLine());
                if (hours < 0 || hours > 23)
                {
                    Console.WriteLine("некорректный ввод");
                    return;
                }
                int minutes = int.Parse(Console.ReadLine());
                if (minutes < 0 || minutes > 59)
                {
                    Console.WriteLine("некорректный ввод");
                    return;
                }
                ticket.DispatchTime = DateTime.Now.AddHours(hours);
                ticket.DispatchTime = DateTime.Now.AddMinutes(minutes);
                while (ticket.DispatchTime > DateTime.Now.AddDays(3))
                {
                    hours = int.Parse(Console.ReadLine());
                    if (hours < 0 || hours > 23)
                    {
                        Console.WriteLine("некорректный ввод");
                        return;
                    }
                    minutes = int.Parse(Console.ReadLine());
                    if (minutes < 0 || minutes > 59)
                    {
                        Console.WriteLine("некорректный ввод");
                        return;
                    }
                    ticket.DispatchTime = DateTime.Now.AddHours(hours);
                    ticket.DispatchTime = DateTime.Now.AddMinutes(minutes);
                }
                int TicketCount = int.Parse(Console.ReadLine());
                if (TicketCount < 1)
                {
                    Console.WriteLine("некорректный ввод");
                    return;
                }
                db.Add(ticket);
                db.SaveChanges();
                transaction.Commit();
            }
        }
        public static void BuyTicket(ApplicationDbContext db, int id)
        {
            int ticketid = int.Parse(Console.ReadLine());
            Ticket ticket = db.Tickets
                .FirstOrDefault(x => x.ID == ticketid);
            if(ticket.TicketCount == 0)
            {
                Console.WriteLine("некорректный ввод");
                return;
            }
            if(ticket.Route == null)
            {
                Console.WriteLine("некорректный ввод");
                return;
            }
            if(ticket.DispatchTime < DateTime.Now)
            {
                Console.WriteLine("некорректный ввод");
                return;
            }
            User user = db.Users
                .FirstOrDefault(x => x.ID == id);
            int input = int.Parse(Console.ReadLine());
            while(input <= 0 || input > ticket.TicketCount)
            {
                if (input <= 0)
                {
                    Console.WriteLine("данные некорректны");
                    input = int.Parse(Console.ReadLine());
                }
                if(input > ticket.TicketCount)
                {
                    Console.WriteLine("данные некорректны");
                    input = int.Parse(Console.ReadLine());
                }
            }
            ticket.TicketCount -= input;
            Order order = new Order
            {
                UserID = user.ID,
                TicketID = ticket.ID,
                CountOfTicket = input,
                TotalPrice = input * ticket.Price,
                OrderStatus = enums.OrderStatus.Bought
            };
            db.Add(order);
            db.Update(ticket);
            db.SaveChanges();
        }
    }    
}