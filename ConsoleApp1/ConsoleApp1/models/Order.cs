﻿using ApplicationDbContext.enums;
using ApplicationDbContext.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ApplicationDbContext.models
{
    public class Order : IEntity<int> // order наследуется от IEntity для получения Id
    {
        public int ID { get; set; } //Id заказа
        public DateTime DateOfOrder { get { return DateTime.Now; } set { } } // дата заказа
        [Required, ForeignKey("User")]
        public int UserID { get; set; } // Id пользователя совершившего заказ
        public virtual User User { get; set; } // 
        [Required, ForeignKey("Ticket")]
        public int TicketID { get; set; } // Id билета
        [Required]
        public int CountOfTicket { get; set; } // число билетов
        [Required]
        public decimal TotalPrice { get; set; } // сумма стоимости билетов
        [Required]
        public OrderStatus OrderStatus { get; set; } // статус заказа
    }
}
